#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h> 

#include <pthread.h>
#include <time.h>


#define TYPE_UDP

//#define DATA_SIZE 640
char * p_IN_PCM_Buff;
char * p_OUT_PCM_Buff;

typedef struct sockaddr SOCKADDR;


//#define TYPE_TCP

int sockfd = 0;
struct sockaddr_in serv_addr;
int tosize =  sizeof(serv_addr);

int32_t len_byte = 0;
FILE *fin;
int run = 1;

void sleep_ms(int milliseconds)
{
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
}


void *IN_PCM_thread( void *ptr )
{

     int n = 0;
     int len;

     while (run)
     {
         len = fread(p_IN_PCM_Buff, sizeof(char), len_byte, fin);
         if(len != len_byte){
        	 run = 0;
        	 break;
         }

         //printf("Read %d \n", len);
     #ifdef TYPE_TCP
         send(sockfd, IN_PCM_Buff, len, 0);
     #else
         sendto(sockfd, p_IN_PCM_Buff, len, 0, (SOCKADDR *)&serv_addr, tosize);
     #endif

#if 0
#ifdef TYPE_TCP
       n = recv(sockfd, OUT_PCM_Buff, len, 0);
   #else
       n = recvfrom(sockfd, OUT_PCM_Buff, len, 0, (SOCKADDR *)&serv_addr,(socklen_t *) &tosize);
   #endif
       if(n > 0){
           //printf("Read %d\n", n);
           fwrite(OUT_PCM_Buff, sizeof(char), DATA_SIZE, stdout);
       }

       if(n < 0)
       {
           printf("\n Read error \n");

       }
#else



    	 sleep_ms(19);
#endif
     }


     return 0;
}

void *OUT_PCM_thread( void *ptr )
{
//     char *message;
//     message = (char *) ptr;
//     printf("%s \n", message);
     int n;

     while (run)
     {
     #ifdef TYPE_TCP
         n = recv(sockfd, PCM_Buff, len, 0);
     #else
         n = recvfrom(sockfd, p_OUT_PCM_Buff, len_byte, 0, (SOCKADDR *)&serv_addr,(socklen_t *) &tosize);
     #endif
    	 //printf("Read %d \n", n);

         if(n > 0){
             //printf("Read %d\n", n);
             fwrite(p_OUT_PCM_Buff, sizeof(char), n, stdout);
         }

//         if(n != len_byte)
//         {
//    		 break;
//         }
     }

     return 0;
}

void *Maintain_UDP( void *ptr )
{
    int len = 0;
    char Buff[0];

    while (run)
    {
        if(sendto(sockfd, Buff, len, 0, (SOCKADDR *)&serv_addr, tosize) != 0){
        	fprintf( stderr, "Maintain_UDP FAIL\n");
        }
        sleep(1);
    }


    return 0;
}

int main(int argc, char *argv[])
{
    int args;

    pthread_t thread1, thread2, thread3;
//    char *message1 = "IN_PCM_thread";
//    char *message2 = "OUT_PCM_thread";
    int  iret1, iret2, iret3;
    char *inFile;


    if(argc == 3)
    {
    	//printf("Receive\n");
    }else if (argc == 4)
    {
    	//printf("Send\n");
    }else{
        printf("\n Usage: %s <ip of server> <data_len_in_byte>  <file to stream>\n",argv[0]);
        return 1;
    } 


    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        printf("\n Error : Could not create socket \n");
        return 1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr)); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(4242); 

    args = 1;
    if(inet_pton(AF_INET, argv[args], &serv_addr.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
        return 1;
    } 
    args++;

    len_byte = atol(argv[args]);


    if(argc == 4){ //Send
        inFile = argv[argc-1];
        fin = fopen(inFile, "rb");
        if (!fin)
        {
            fprintf (stderr, "Could not open input file %s\n", argv[argc-2]);
            return EXIT_FAILURE;
        }

        p_IN_PCM_Buff = malloc(len_byte*sizeof(char));

        iret1 = pthread_create( &thread1, NULL, IN_PCM_thread, (void*) NULL);
        pthread_join( thread1, NULL);
        printf("Thread 1 returns: %d\n",iret1);
        fclose(fin);

    }else {//Receive
        p_OUT_PCM_Buff = malloc(len_byte*sizeof(char));

        iret2 = pthread_create( &thread2, NULL, OUT_PCM_thread, (void*) NULL);
        iret3 = pthread_create( &thread3, NULL, Maintain_UDP, (void*) NULL);

        pthread_join( thread2, NULL);
        pthread_join( thread3, NULL);
        printf("Thread 2 returns: %d\n",iret2);
        printf("Thread 2 returns: %d\n",iret3);
    }


    exit(0);

}
